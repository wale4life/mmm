<?php

namespace mmm;

use Illuminate\Database\Eloquent\Model;

class AdminRequest extends Model
{
    protected $fillable = [ 'admmin_id', 'next_ph', 'is_master'  ];


    public function admin() {
    	return $this->belongsTo(mmm\Admin::class);
    }
}
