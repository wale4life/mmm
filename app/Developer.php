<?php

namespace mmm;

use Illuminate\Database\Eloquent\Model;
use mmm\User;

class Developer extends Model
{
    //Receivestatus is either WAITING|RECEIVED
    protected $fillable = [ 'user_id', 'status' ];


	public function users() {
		return $this->hasOne(User::class);
	}

	public function user() {
		return $this->belongsTo(User::class, 'user_id');
	}
}
