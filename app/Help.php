<?php

namespace mmm;

use Illuminate\Database\Eloquent\Model;

class Help extends Model
{
    protected $fillable = [ 'amount' ];

    public function helps() {
    	return $this->hasMany(mmm\HelpProvide::class);
    }
}
