<?php

namespace mmm;

use Illuminate\Database\Eloquent\Model;
use mmm\User;
use mmm\Help;

class HelpProvide extends Model
{
    protected $fillable = [
    	'reciever_id',
    	'sender_id',
    	'help_id',
    	'status',
    ];

    public function receiver() {
    	return $this->belongsTo(User::class, 'reciever_id');
    }

    public function sender() {
    	return $this->belongsTo(User::class, 'sender_id');
    }

    public function help() {
    	return $this->belongsTo(Help::class);
    }

    public function scopePendingSent($query, User $user) {
    	return $query->where([ 'status' => 'PENDING', 'sender_id' => $user->id ]);
    }

    public function scopePendingRecieved($query, User $user) {
    	return $query->where([ 'status' => 'PENDING', 'reciever_id' => $user->id ]);
    }

    public function scopeConfirmed($query, User $user) {
        return $query->where([ 'status' => 'CONFIRMED', 'reciever_id' => $user->id ]);
    }

    public function scopeAllSent($query, User $user) {
        return $query->where([ 'sender_id' => $user->id ]);
    }
    
    /**
     * Check pending payment
     *
	 * @param User $user
	 * @return boolean
     */
    public function hasPending(User $user) {
    	return !!$this->where([ 'status' => 'PENDING', 'sender_id' => $user->id ])->first();
    }
}
