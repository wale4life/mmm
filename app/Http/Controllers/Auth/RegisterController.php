<?php

namespace mmm\Http\Controllers\Auth;

use mmm\User;
use mmm\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use mmm\Repo\MatrixFlow;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/payment-details';

    private $ref = '';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->ref = request()->get('ref');
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
       // $data['referrer'] = $this->ref;
        return Validator::make($data, [
            'name' => 'required|max:255',
            'username' => 'required|max:30',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:4|confirmed',
            'phone' => 'required|numeric',
           // 'referrer' => 'nullable|exists:users,username',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        //$referal_url = url("/{$data['username']}");
        $user =  User::create([
            'name' => $data['name'],
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'phone' => $data['phone'],
           // 'referral_url' => $referal_url,
           // 'referrer' => $data['referrer'],
        ]);


        return $user;
    }
}
