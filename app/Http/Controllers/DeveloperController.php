<?php

namespace mmm\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use mmm\Matrix;
use mmm\User;

class DeveloperController extends Controller
{

	public function __construct() {
		$this->middleware('auth');
        // $dev = auth()->user()->developer;
        // $this->authorize('requestPayment', $dev);
	}
    
    /**
     * SHows the Matrix Settings Form
     */
    public function matrix() {


    	$matrix = Matrix::first();
    	return view('master/matrix', [ 'matrix' => $matrix ]);
    }

    /**
     *	Update the matrix Details
     */
    public function updateMatrix(Request $request) {

    	$this->validate($request, [
    		'number_of_pairs' => 'required|numeric',
		]);

		$matrix = Matrix::firstOrNew([
			'number_of_pairs' => $request->number_of_pairs
		]);

		$matrix->number_of_pairs = $request->number_of_pairs;
		$matrix->save();


        Session::flash('msg', 'System Matrix has been updated has been saved successfully!');
        Session::flash('type', 'success');

        return redirect('master/matrix');

    }

    public function admin() {

        $bunch = \mmm\User::pluck('username', 'id')->all();
        return view('master/admin', [ 'users' => $bunch ]);

    }

    public function makeAdmin(Request $request) {

        $this->validate($request, [
            'user' => 'required|numeric',
        ]);

        //$this->authorize();

        $user = \mmm\User::find($request->user);
        $admin = \mmm\Admin::where('user_id', $request->user)->first();
        $dev = \mmm\Developer::where('user_id', $request->user)->first();

        if($dev) {

            Session::flash('msg', 'The User is already an admin');
            Session::flash('type', 'warning');

            return redirect('master/admins');
        }

        if( !$admin ) {

            \mmm\Admin::create([
                'user_id' => $user->id,
                'receive_status' => 'WAITING',
            ]);

            Session::flash('msg', 'Admin successfully Added');
            Session::flash('type', 'success');

            return redirect('master/admins');
        } 


        Session::flash('msg', 'The User is already an admin');
        Session::flash('type', 'warning');

        return redirect('master/admins');
        
    }

}
