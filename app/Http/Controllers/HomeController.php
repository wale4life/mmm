<?php

namespace mmm\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use \mmm\PaymentDetail;
use \mmm\Admin;
use \mmm\Developer;
use \mmm\HelpProvide;
use \mmm\User;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        return view('dashboard');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        return view('profile')->with('user', auth()->user());
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function support()
    {
        return view('support');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function matrix()
    {

        return view('matrix');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getHelp()
    {
        $payment = PaymentDetail::whereUserId(auth()->user()->id)->first();
        $helpProvide = HelpProvide::pendingRecieved(auth()->user())->first();
        $helpConfirmed = HelpProvide::confirmed(auth()->user()->first())->latest()->paginate(100);

        return view('get-help', [ 
                'payment' => $payment, 
                'helpProvide' => $helpProvide, 
                'confirms' => $helpConfirmed,
        ]);
    }

    /**
     * Help Receiver confirmed reciept of the help
     * @return 
     */
    public function getHelpConfirmed(Request $request) {
        $this->validate($request, [
            'hp' => 'required',
        ]);

        $hp = HelpProvide::findOrFail($request->hp);
        $hp->status = 'CONFIRMED';
        $hp->save();

        $user = $hp->receiver;
        $user->status = 'WAITING';
        $user->save();


        Session::flash('msg', 'You have successfully confirmed that you have received the payment');
        Session::flash('type', 'success');
        return redirect(url('gh'));
    }

    /**
     *  Admin request payment interface
     */
    public function nextPayment() {
        $admin = auth()->user()->admin;
        $this->authorize('requestPayment', $admin);

        return view('admin/requestPayment');
    }

    /**
     * Payment details interface
     */
    public function paymentDetails() {

        $payment = PaymentDetail::whereUserId(auth()->user()->id)->first();

        return view('payment-details', [ 'payment' => $payment]);

    }

    /**
     *  Save Payment Details
     */
    public function savePaymentDetails(Request $request) {

        $this->validate($request, [
            'bank' => 'required',
            'account_type' => 'required',
            'account_name' => 'required',
            'account_number' => 'required|numeric'

        ]);

        //Get users payment details
        $payment = PaymentDetail::whereUserId(auth()->user()->id)->first();

        if($payment) {
            $payment = $payment->update([
                'bank' => $request->bank,
                'account_type' => $request->account_type,
                'account_name' => $request->account_name,
                'account_number' => $request->account_number,
            ]);
        } else {
            $payments = PaymentDetail::create([
                'bank' => $request->bank,
                'account_type' => $request->account_type,
                'account_name' => $request->account_name,
                'account_number' => $request->account_number,
                'user_id' => auth()->user()->id,
            ]);
        }

        Session::flash('msg', 'Your payment details has been saved successfully!');
        Session::flash('type', 'success');

        return redirect(route('paymentDetailsForm'));
    }

    public function provideHelp(Request $request) {

        $this->validate($request, [
            'amount' => 'required|numeric',
        ]);

        $help = \mmm\Help::find($request->amount);

        $hp = new \mmm\HelpProvide();
        $hp->sender_id = auth()->user()->id;
        $hp->help_id = $help->id;
        $hp->status = 'pending';

        $user = null;

        //checks if an Admin wants to forcefully claim the next payment
        if(Admin::where('receive_status', 'FORCED')->count() > 0) {
            $admin = Admin::where('receive_status', 'FORCED')->first();
            $user = $admin->user;
            $admin->receive_status = 'RECEIVED';
            $admin->save();
        } else if(Developer::where('status', 'FORCED')->count() > 0) {
            $dev = Developer::where('status', 'FORCED')->first();
            $user = $dev->user;
            $dev->status = 'RECEIVED';
            $dev->save();
        }

        //Check if the user is providing help for the first time and assign to admin
        else if( !auth()->user()->helpProvides ) {
            
            //Assign provided help to admin
            $admins = \mmm\Admin::where('receive_status', 'WAITING')->pluck('id');

            //If no admin is waiting check if all admins have receive payment
            if(Admin::where('receive_status', 'RECEIVED')->count() > 0 && (count($admins) <= 0) ) {
                $user = Developer::first()->user;
                $admins = Admin::where('receive_status', 'RECEIVED')->each(function($x) {
                    $x->receive_status = 'WAITING';
                    $x->save();
                });
            } else {
                $admin = count($admins) <= 0 ? Admin::first() : Admin::find($admins[0]);
                $user = $admin->user;
            }

            $user = $admin->user;
            $admin->status = 'RECEIVED';
            $admin->save();

        } else {
            
            //Randomly fatch a user to Assign PH
            $possib = [];
            $users = User::whereStatus('WAITING')->chunk(100, function($users) use($possib) {
                
                foreach($users as $user) {
                    if( $user->helpProvides()->whereStatus('CONFIRMED')->count() > 0 ) {
                        if($user->id === auth()->user()->id) continue;
                        $possib[] = $user;
                    }
                }

            });

            //Assign payment to user
            if( count($possib) > 0 ) {
                $user = $possib[rand(0, count($possib)-1)];
                $user->status = 'RECEIVED';
                $user->save();
            } else {
                $admin = \mmm\Admin::where('receive_status', 'WAITING')->first();
                $user = $admin->user;
                $user->status = 'RECEIVED';
                $user->save();
            }
        }

        $hp->reciever_id = $user->id;
        $hp->save();

        Session::flash('msg', 'You are providing help of NGN' . $request->amount . ', ensure to complete your payment within the next 24hrs');
        Session::flash('type', 'success');

        $dev = Developer::first();
        $dev->status = 'WAITING';
        $dev->save();

        return redirect(url('ph'));
    }

    public function provideHelpView() {
        $help = \mmm\Help::pluck('amount', 'id');
        $helpProvide = HelpProvide::pendingSent(auth()->user())->first();
        $helpRecieved = HelpProvide::AllSent(auth()->user()->first())->latest()->paginate(100);

        return view('provide-help', [ 
            'help_money' =>$help, 
            'helpProvide' => $helpProvide ,
            'helpProvides' => $helpRecieved,
        ]);
    }
}
