<?php

namespace mmm\Http\Controllers;

use Illuminate\Http\Request;
use mmm\User;

class ProfileController extends Controller
{
    //
    public function store(Request $request){

      $this->validateData($request);

      if($this->inputedAnotherPersonEmail($request['email'])){
        return $this->respond('sorry email taken', 'warning', '/profile');
      }

      if($this->updateProfile($request, $this->uploadPhoto($request))){
        return $this->respond('Your profile has been updated successfully!',
         'success', '/profile');
      }

      return $this->respond('Sorry your profile could not be updated', 'danger', '/profile');
    }

    private function validateData(Request $request){
      return $this->validate($request, [
        'name' => 'required|max:255',
        'email' => 'required|email|max:255',
        'username' => 'required|max:30:',
        'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'phone' => 'numeric',
      ]);
    }

    private function inputedAnotherPersonEmail($email) {
      return ((auth()->user()->email != $email) && (User::whereEmail($email)->first() != null));
    }

    private function uploadPhoto(Request $request){
      if( $request->photo ) {
            $imageName = auth()->user()->id.'.'.$request->photo->getClientOriginalExtension();
            $request->photo->move(public_path('photos'), $imageName);
            return '/photos/'.$imageName;
      }
    }

    private function updateProfile(Request $request, $photoUrl) {
      return auth()->user()->update([
        'name' => $request['name'],
        'username' => $request['username'],
        'email' => $request['email'],
        'bio' => $request['bio'],
        'photo' => $photoUrl,
        'phone' => $request->phone,
        'address' => $request['address'],
        'state' => $request['state'],
        'city' => $request['city'],
      ]);
    }

    private function respond($msg, $type, $targetUrl){
      \Session::flash('msg', $msg);
      \Session::flash('type', $type);
      return redirect($targetUrl);
    }

    public function savePhone(Request $request) {
      $this->validate($request, [
          'phone' => 'required',
      ]);


      $user = auth()->user();
      $user->phone = $request->phone;
      $user->save();


      \Session::flash('msg', 'You have successfully updated your phone Number');
      \Session::flash('type', 'success');
      return redirect('payment-details');
    }
}
