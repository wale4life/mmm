<?php

namespace mmm;

use Illuminate\Database\Eloquent\Model;

interface IRole
{
    /** @var ADMIN */
    const ADMIN = 'ADMIN';

    /** @var BASIC */
    const BASIC = 'BASIC';

    /** @var DEV */
    const DEV = 'DEV';

}
