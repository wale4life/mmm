<?php

namespace mmm;

use Illuminate\Database\Eloquent\Model;

class Matrix extends Model
{
    protected $fillable = [
    	'number_of_pairs',
    	'levels',
    ];

    protected $table = 'matrix';
}
