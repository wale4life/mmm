<?php

namespace mmm;

use Illuminate\Database\Eloquent\Model;
use mmm\User;

class PaymentDetail extends Model
{

    protected $guarded = [
    	'id'
    ];



	public function users() {
		return $this->hasMany(User::class);
	}

	public function user() {
		return $this->belongsTo(User::class);
	}
}
