<?php

namespace mmm\Policies;

use mmm\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdminPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function requestPayment(User $user, Admin $admin) {
        
        return $admin->user_id === $user->id;
    }
}
