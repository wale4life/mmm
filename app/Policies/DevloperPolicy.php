<?php

namespace mmm\Policies;

use mmm\User;
use mmm\Developer;
use Illuminate\Auth\Access\HandlesAuthorization;

class DevloperPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the developer.
     *
     * @param  \mmm\User  $user
     * @param  \mmm\Developer  $developer
     * @return mixed
     */
    public function view(User $user, Developer $developer)
    {
        dd($developer);
        return $developer->user_id === $user->id;
    }

    /**
     * Determine whether the user can create developers.
     *
     * @param  \mmm\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $developer->user_id === $user->id;
    }

    /**
     * Determine whether the user can update the developer.
     *
     * @param  \mmm\User $user
     * @param  \mmm\Developer $developer
     * @return mixed 
     */
    public function update(User $user, Developer $developer)
    {
        //
    }

    /**
     * Determine whether the user can delete the developer.
     *
     * @param  \mmm\User  $user
     * @param  \mmm\Developer  $developer
     * @return mixed
     */
    public function delete(User $user, Developer $developer)
    {
        //
    }

    
    public function before(User $user, $abilities) {

        $role = \mmm\Role::whereName('DEV')->first();
        return $user->role->id === $role->id;
    }
}
