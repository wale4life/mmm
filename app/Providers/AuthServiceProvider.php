<?php

namespace mmm\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'mmm\Model' => 'mmm\Policies\ModelPolicy',
        mmm\Admin::class => mmm\Policies\AdminPolicy::class,
        mmm\Developer::class => mmm\Policies\DeveloperPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
