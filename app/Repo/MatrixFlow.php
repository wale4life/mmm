<?php

namespace mmm\Repo;

use Illuminate\Database\Eloquent\Model;
use mmm\User;
use mmm\Matrix;
use mmm\AvailableForPair;
use mmm\PairedUser;
use mmm\PairedMatrix;
use mmm\WillBePaired;


class MatrixFlow 
{
    
	public function addToAvailable(User $user, $amount) {

		return AvailableForPair::create([
			'user_id' => $user->id,
			'amount' => $amount,
			'status' => 'waiting',
		]);
	}


	/**
	 * @return WillBePaired
	 */
	private function addForPair(User $user, $amount) {

		return WillBePaired::create([
			'user_id' => $user->id,
			'amount' => $amount,
			'status' => 'waiting',
		]);

	}

	/**
	 * @return PairedUser
	 */
	public function pairUser(User $user, $amount) {

		$willBePaired = $this->addForPair($user, $amount);


		$matrix = Matrix::first();

		$no_of_pairs = 2;
		if($matrix) {
			$no_of_pairs = $matrix->number_of_pairs;
		}

		$pairedUser = PairedUser::create([ 'user_id' => $user->id ]);

		for($i=1; $i<=$no_of_pairs; $i++) {
			$pairedMatrix = PairedMatrix::create([
				'paired_user_id' => $pairedUser->id,
				'will_be_paired_id' => $willBePaired->id,
				'payment_verified' => false,
				'has_paid' => false,
			]);
		}

		return $pairedUser;

	}


}
