<?php

namespace mmm;

use Illuminate\Database\Eloquent\Model;

class Role extends Model implements IRole
{

    protected $fillable = [ 'name' ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user() {
        return $this->hasOne(User::class);
    }

    /**
     * @return Admin|Author|Staff
     */
    public function userRoleInstance() {

        switch(ucwords(ucfirst($this->name))) {
            case Role::ADMIN:
                return new Admin();
                break;
            case Role::DEVELOPER:
                return new Developer();
                break;
            case Role::AUTHOR:
                return new Volunteer();
                break;
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function admin() {
        return $this->belongsTo(\mmm\Admin::class);
    }  

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function developer() {
        return $this->belongsTo(\mmm\Developer::class);
    }  
}
