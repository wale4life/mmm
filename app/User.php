<?php

namespace mmm;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use mmm\Admin;
use mmm\Role;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * NB: status could either be WAITING|RECEIVED
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password', 'referral_url', 'referrer',
        'bio', 'photo', 'address', 'state', 'city', 'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function admin() {
        return $this->belongsTo(Admin::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function developer() {
        return $this->belongsTo(\mmm\Developer::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role() {
        return $this->belongsTo(Role::class);
    }

    public function helpProvides() {
        return $this->hasMany(\mmm\HelpProvide::class, 'sender_id');
    }

    public function helpReceives() {
        return $this->hasMany(\mmm\HelpProvide::class, 'reciever_id');
    }

    public function paymentDetails() {
        return $this->hasOne(\mmm\PaymentDetail::class);
    }
}
