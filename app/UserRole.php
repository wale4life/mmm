<?php

namespace mmm;

use Illuminate\Database\Eloquent\Model;

abstract class UserRole extends Model implements IRole
{
    protected $fillable = ['user_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user() {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne

    public function role() {
        return $this->hasOne(Role::class);
    }   */

    /**
     * Gets the role type for the user role
     * @return string
     */
    abstract function getRoleType();
}
