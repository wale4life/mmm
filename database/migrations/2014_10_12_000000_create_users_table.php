<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('role_id')->nullable()->references('roles')->on('id');
            $table->string('name');
            $table->string('email', 50)->unique();
            $table->string('username', 50)->unique();
            $table->string('password');
            $table->text('bio')->nullable();
            $table->string('photo')->nullable();
            $table->string('phone', 25)->nullable();
            $table->string('address')->nullable();
            $table->string('state')->nullable();
            $table->string('city')->nullable();
            $table->string('referrer')->nullable();
            $table->string('referral_url')->nullable();
            $table->boolean('blocked')->nullable()->default(0);
            $table->string('status', 30)->nullable()->default('WAITING');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
