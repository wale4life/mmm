<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHelpProvidesTableMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('help_provides', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reciever_id')->references('users')->on('id')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('sender_id')->references('users')->on('id')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('help_id')->references('helps')->on('id')->onDelete('cascade')->onUpdate('cascade');

            $table->string('status')->default('waiting');
            $table->string('proof')->nullable();
            $table->boolean('sender_confirmed')->default(false)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('help_provides');
    }
}
