@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h3>Get Help</h3></div>


            @if($helpProvide)

                <div class="alert alert-info">
                    Make sure you receive the payment before conformation
                </div>
                <!-- Pending Payment -->
                <div class="panel panel-yellow" style='margin: 20px'>
                    <div class="panel-heading panel-green">
                        Your Will be Paid: <span class="pull-right bold">NGN{{ $helpProvide->help->amount }}</span>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-5 well" style='margin:20px'>
                                <h4>You Get Paid By</h4>
                                <p>Name: {{ $helpProvide->sender->name }}</p>
                                <p>Phone: {{ $helpProvide->sender->phone }}</p>
                                <hr class='seperator'>
                                <h4>Proof of Payment</h4>
                                @if($helpProvide->proof) 
                                    <img src="{{ $helpProvide->proof }}">
                                @else
                                    <p>No Proof of payment</p>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <h1>NGN15000</h1>
                                <p>Payment to receive</p>
                                <hr>

                                {{ Form::open([ 'url' => url('gh') ]) }}
                                <input type="hidden" name="hp" value='{{ $helpProvide->id }}'>
                                <div class="form-group">
                                    <label for='confirmed' class='checkbox-inline'>
                                        <input type="checkbox" name="confirmed" value='true' required />
                                        I have recieve the payment
                                    </label>
                                </div>
                                <button class='btn btn-default' type='submit'><i class="fa fa-send"></i> Confirmed Recieved Payment</button>

                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            @else 
                <div class="alert alert-default">
                    Start receiveing help by first <a href="{{ url('ph') }}">providing one</a>
                </div>
            @endif

            </div>
            {{-- Display Previous Help Provided --}}
           @include('includes/help-table', [ 
                'header' => 'Previous Help Received', 
                'confirms' => $confirms,
             ])
        </div>
    </div>
</div>
@endsection
