
@if(Session::has('msg'))
    
    {{-- Success Alert --}}
    @if(Session::get('type') == 'success')
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ Session::get('msg') }}
        </div>
    @elseif(Session::get('type') == 'info')

    {{-- Info Alert --}}
    {{-- @if(Session::get('type') == 'success') --}}
        <div class="alert alert-info alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ Session::get('msg') }}
        </div>
    {{-- @endif --}}

    {{-- Warning Alert --}}
    @elseif(Session::get('type') == 'warning')
        <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ Session::get('msg') }}
        </div>
    @elseif(Session::get('type' == 'danger'))
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ Session::get('msg') }}
        </div>
    @endif
    
@endif