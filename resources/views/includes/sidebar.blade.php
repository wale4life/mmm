
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        {{-- <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!-- /input-group -->
                        </li> --}}
                        <li>
                            <a href="{{ url('/dashboard') }}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="{{ url('/profile') }}"><i class="fa fa-edit fa-fw"></i> Profile <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ url('/payment-details') }}">
                                        <i class="fa fa-edit fa-fw"></i> Payment Details
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('/profile') }}">
                                        <i class="fa fa-edit fa-fw"></i> User Profile
                                    </a>
                                </li>
                               
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        {{-- <li>
                            <a href="#"><i class="fa fa-rocket fa-fw"></i> Upgrade</a>
                        </li> --}}
                        <li>
                            <a href="{{ url('#payments') }}"><i class="fa fa-money fa-fw"></i> Payments <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ url('/ph') }}">
                                        <i class="fa fa-money fa-fw"></i> Provide Help (PH)
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('/gh') }}">
                                        <i class="fa fa-money fa-fw"></i> Get Help (GH)
                                    </a>
                                </li>
                            </ul>
                        </li>
                        {{-- <li>
                            <a href="{{ url('/matrix') }}"><i class="fa fa-sitemap fa-fw"></i> Matrix</a>
                        </li> --}}
                        <li>
                            <a href="{{ url('/support') }}"><i class="fa fa-smile-o fa-fw"></i> Help & Support</a>
                        </li>
                        <li>
                            <a href="#xme"><i class="fa fa-gears fa-fw"></i> Admins<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ url('/admins/next-payment') }}">
                                    <i class="fa fa-money fa-fw"></i> Request Next Reg
                                    </a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        @cannot('view', Developer::class)
                            <li>
                                <a href="#url"><i class="fa fa-gears fa-fw"></i> Developers<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="{{ url('/master/next-payment') }}">
                                        <i class="fa fa-key fa-fw"></i> Maintain App</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/master/matrix') }}">
                                        <i class="fa fa-sitemap fa-fw"></i> Configure Matrix</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/master/admins') }}">
                                        <i class="fa fa-users fa-fw"></i> Admins</a>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                        @endcannot
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->