@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h3>Manage Admins </h3></div>

                <div class="panel-body">

                    <div>
                        {{ Form::model([ 'url' => url('master/admin')]) }}
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for='user'>Choose User to make Admin:</label>
                                {{ Form::select('user', $users, null, [ 'class' => 'form-control', 'placeholder' => '-- Choose a user --', 'required' ]) }}

                                @if ($errors->has('user'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('user') }}</strong>
                                    </span>
                                @endif
                            </div>


                            <button class='btn btn-primary' type='submit'>Make Admin</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
