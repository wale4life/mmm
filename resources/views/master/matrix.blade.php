@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h3>System Matrix Info </h3></div>

                <div class="panel-body">

                    <div>
                        {{ Form::model($matrix, [ 'url' => url('master/matrix')]) }}
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for='account_number'>Number Of Pairs:</label>
                                {{ Form::number('number_of_pairs', null, ['placeholder' => 'Number of Pairs', 'required', 'class' => 'form-control']) }}

                                @if ($errors->has('number_of_pairs'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('number_of_pairs') }}</strong>
                                    </span>
                                @endif
                            </div>


                            <button class='btn btn-primary' type='submit'>Save Details</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
