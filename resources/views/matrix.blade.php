@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h3>Your Matrix</h3></div>

                <div class="panel-body">
                    <h4>Your Total Earnings: <span class="currency">0.00 NGN</span></h4>
                </div>

                <div class="panel-body">
                    <div class="well well-lg">
                     
                        <p>You have not added any payment Details</p>
                     
                        <h4>Payment Account</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
