@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h3>Payment Details </h3></div>

                <div class="panel-body">
                    <div class="alert alert-info">
                        Provide the account details for all your payments
                    </div>

                    <div>
                        {{ Form::model($payment, [ 'url' => url('payment-details')]) }}
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for='bank'>Bank Name:</label>
                                {{ Form::text('bank', null, [ 'placeholder' => 'Bank Name', 'class' => 'form-control', 'required']) }}
                                @if ($errors->has('bank'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('bank') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for='account_type'>Account Type:</label>
                                {{ Form::select('account_type', ['Savings' => 'Savings', 'Current' => 'Current'], null, [ 'class' => 'form-control', 'placeholder' => '-- Choose Your Account Type --', 'required' ]) }}

                                @if ($errors->has('account_type'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('account_type') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for='account_name'>Account Name:</label>
                                {{ Form::text('account_name', null, ['placeholder' => 'Account Name', 'required', 'class' => 'form-control']) }}

                                @if ($errors->has('account_name'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('account_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for='account_number'>Account Number:</label>
                                {{ Form::number('account_number', null, ['placeholder' => 'Account Number', 'required', 'class' => 'form-control']) }}

                                @if ($errors->has('account_number'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('account_number') }}</strong>
                                    </span>
                                @endif
                            </div>


                            <button class='btn btn-primary' type='submit'>Save Details</button>
                        </form>
                    </div>
                </div>
            </div>

                        <div class="panel panel-default">
                <div class="panel-heading"><h3>Contact Details </h3></div>

                <div class="panel-body">
                    <div>
                        {{ Form::model(auth()->user(), [ 'url' => url('update-phone')]) }}
                            <div class="form-group">
                                <label for='phone'>Phone Number:</label>
                                {{ Form::number('phone', null, ['placeholder' => 'Phone Number', 'required', 'class' => 'form-control']) }}

                                @if ($errors->has('phone'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>


                            <button class='btn btn-primary' type='submit'>Save Phone</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
