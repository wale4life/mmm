@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h3>Your Profile </h3></div>

                <div class="panel-body">
                    <div class="alert alert-info">
                        You can edit you profile here.
                    </div>

                    <div>
                        {{ Form::model($user, [ 'url' => url('/profile'), 'enctype' => 'multipart/form-data']) }}
                            {{ csrf_field() }}
                            <div class="form-group">
                              <label for='photo'>Photo:</label><br/>
                              <img id="photoPreview" src="{{ $user->photo }}" style="width:150px; height: 150px;"/>
                              <input type="file" name="photo" class="form-control" id="photo">
                            </div>
                            @if ($errors->has('avatar'))
                                <span class="help-block" style="color: red;">
                                    <strong>{{ $errors->first('avatar') }}</strong>
                                </span>
                            @endif
                            <div class="form-group">
                                <label for='bank'>Name:</label>
                                {{ Form::text('name', null, [ 'placeholder' => 'Name', 'class' => 'form-control' ]) }}
                                @if ($errors->has('name'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for='account_type'>Email:</label>
                                {{ Form::email('email', null, [ 'placeholder' => 'email', 'class' => 'form-control' ]) }}

                                @if ($errors->has('email'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            {{-- <div class="form-group">
                                <label for='username'>User Name:</label>
                                {{ Form::text('username', null, ['placeholder' => 'Username', 'required', 'class' => 'form-control']) }}

                                @if ($errors->has('username'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div> --}}
                            <div class="form-group">
                                <label for='Phone'>Phone:</label>
                                {{ Form::text('Phone', null, ['placeholder' => 'Phone', 'required', 'class' => 'form-control']) }}

                                @if ($errors->has('Phone'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('Phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for='account_number'> Bio:</label>
                                {{ Form::textarea('bio', null, ['placeholder' => 'About Me', 'class' => 'form-control']) }}

                                @if ($errors->has('bio'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('bio') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for='address'> Address:</label>
                                {{ Form::textarea('address', null, ['placeholder' => 'Address', 'class' => 'form-control']) }}

                                @if ($errors->has('address'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for='state'> State:</label>
                                {{ Form::text('state', null, ['placeholder' => 'State', 'class' => 'form-control']) }}


                                @if ($errors->has('state'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('state') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for='city'> City:</label>
                                {{ Form::text('city', null, ['placeholder' => 'City', 'class' => 'form-control']) }}

                                @if ($errors->has('city'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <button class='btn btn-primary' type='submit'>Save Details</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
  <script>
  function readURL(input, previewImageId) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(previewImageId).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
  }

  $("#photo").change(function(){
    readURL(this, '#photoPreview');
  });
</script>
@endsection
