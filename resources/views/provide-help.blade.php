@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h3>Provide Help </h3></div>

                <div class="panel-body">
                        @if($helpProvide) 
                            <div class="alert alert-warning">
                                <p>You cannot provide another help until you complete the pending payment</p>
                            </div>
                        @else
                            <div class="alert alert-info">
                              <p>Once payment is made you have limited time to complete the payment</p>
                            </div>
                        @endif

                    <div>
                        {{ Form::open( [ 'url' => url('ph')]) }}
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for='amount'>Select Amount to PH: (NGN)</label>
                                {{ Form::select('amount', $help_money, null, [ 'class' => 'form-control',  'required' ]) }}

                                @if ($errors->has('amount'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('amount') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <button class='btn btn-primary' type='submit'
                             {{ $helpProvide ? 'disabled': '' }}>Provide Help</button>
                        </form>
                    </div> 
                </div>
            </div>

            @if($helpProvide)

                <!-- Pending Payment -->
                <div class="panel panel-yellow" style='margin: 20px'>
                    <div class="panel-heading panel-yellow">
                        Your Pending Payments <span class="pull-right bold">NGN{{ $helpProvide->help->amount }}</span>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-5 well" style='margin:20px'>
                                <h4>You Make Payment To</h4>
                                <p>Name: {{ $helpProvide->receiver->name }}</p>
                                <p>Phone: {{ $helpProvide->receiver->phone }}</p>
                                <hr class='seperator'>
                                <h4>Account Details</h4>
                                <p>Bank Name: {{ $helpProvide->receiver->paymentDetails->bank }}</p>
                                <p>Account Type: {{ $helpProvide->receiver->paymentDetails->account_type }}</p>
                                <p>Account Number: {{ $helpProvide->receiver->paymentDetails->account_number }}</p>
                            </div>
                            <div class="col-md-6">
                                <h1>NGN15000</h1>
                                <p>Due Payment</p>
                                <hr>

                                {{ Form::open([ 'url' => url('ph'), 'files' => true ]) }}
                                <div class="form-group">
                                    <label for='confirm'>Add a proof of payment:</label>
                                    {{ Form::file('confirm', [ 'class' => '']) }}
                                </div>
                                <button class='btn btn-default' type='submit'>
                                    <i class="fa fa-send"></i> Confirm payment
                                </button>

                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            @endif


            {{-- Display Previous Help Provided --}}
           @include('includes/help-table', [ 
                'header' => 'Previous Help Provided',
                'confirms' => $helpProvides,
            ])

        </div>
    </div>
</div>
@endsection
