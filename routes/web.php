<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/dashboard', 'HomeController@dashboard');
Route::get('/profile', 'HomeController@profile');
Route::post('/profile', 'ProfileController@store');
ROute::post('/update-phone', 'ProfileCOntroller@savePhone')->name('save.phone');


Route::get('/gh', 'HomeController@getHelp');
Route::post('/gh', 'HomeController@getHelpConfirmed');
Route::get('/payment-details', 'HomeController@paymentDetails')->name('paymentDetailsForm');
Route::post('/payment-details', 'HomeController@savePaymentDetails');
Route::post('/ph', 'HomeController@provideHelp');
Route::get('/ph', 'HomeController@provideHelpView');


Route::get('/matrix', 'HomeController@matrix');
Route::get('/support', 'HomeController@support');
Route::get('/admins/next-payment', 'HomeController@nextPayment');


//Developer Routes
Route::get('/master/matrix', 'DeveloperController@matrix');
Route::post('/master/matrix', 'DeveloperController@updateMatrix');
Route::get('/master/admins', 'DeveloperCOntroller@admin');
Route::post('/master/admins', 'DeveloperController@makeAdmin');
